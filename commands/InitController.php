<?php

namespace app\commands;

use yii\console\Controller;

class InitController extends Controller
{
    public function actionCreateAdmin()
    {
	$this->actionAddUser ( 'admin', 'pwd' );
        return 0;
    }
    public function actionAddUser($name, $pwd)
    {
		$user = new \app\models\User;
		$user->name = $name;
		$user->email = 'dan@bronyakin.ru';
		$user->salt = \Yii::$app->security->generateRandomString();
		$user->pwdhash = md5 ( md5('pwd').$user->salt );
        $user->save();
        return 0;
    }
    public function actionDelUser($id)
    {
		\app\models\User::findOne($id)->delete();
        return 0;
    }
    public function actionListUsers()
    {
		$users = \app\models\User::find()->all();
		foreach ( $users as $user )
		{
			echo $user->id."\t".$user->name."\n";
		}
        return 0;
    }
    public function actionResetAll()
    {
		\Yii::$app->db->createCommand()->truncateTable(\app\models\Game::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\Gamestate::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\GamePlayer::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\GameResult::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\OverallDisplay::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\Player::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\Results::tableName())->execute();
		\Yii::$app->db->createCommand()->truncateTable(\app\models\User::tableName())->execute();
        return 0;
    }
}
