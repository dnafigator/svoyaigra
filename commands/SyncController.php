<?php

namespace app\commands;

use yii\console\Controller;

class SyncController extends Controller
{
    public function actionPush()
    {
		$manager = new \app\components\SyncManager();
        $manager->push();
        return 0;
    }
}
