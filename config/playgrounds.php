<?php
return [
	'secrets' => [
		1 => '9dcba2f54001371577f120d47575f1ac',
		2 => 'fbeb5afc14c0bd2edfa81a363f93d30b',
		3 => '37e4885abe78e19c248ea58168ec1a0c',
		4 => '3ebdd60e2fbfab828e0a79d48494ae78',
		5 => 'd8f4c3b0791615cd92783bcef0ef644c',
		6 => 'e32466c125bb9fae06721219b722fcb7',
		7 => '801916b0507ce6dc197284d13a3e653e',
		8 => 'db7524f2044bee8174aeb8faff43722f',
		9 => '9507e00ebc2e877549de870753501eb4',
		10 => '7d51608c74c4d01d7578caf0a2aeea22',
		11 => 'd1a354ba18006676d1e638bdc0a7720a',
		12 => '6f8f309160bee88a6158f9821dad9e25',
		13 => '4c40f543c1353fe29de9df25e2ab62a3',
		14 => 'eff9deaf6212a2d00d15256b1d91e342',
		15 => 'cf55a4799bc5f7d09f384bad3ef8a5e8',
		16 => '7e8c0ff0781f8a785e84905bf990daa5',
		99 => 'FAKE',
	],
	'central' => 'http://uec.chgk.info'
//	'central' => 'http://svoyaigra.dev.local'
//	'central' => 'http://si.bronyakin.ru'
 ];