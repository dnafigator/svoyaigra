/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var ii;
var jj;
$(document).ready(function(){
	
	$('#gamesform').on('submit', function(){
		$(':checkbox[name=\'games[]\']').each (function(ind, item){
			if ( item.checked )
			{
				$.ajax({
					url: '/sync/push',
					method: 'get',
					data: {game_id: $(item).val()},
					context: item
				}
				).done(function(data){
					ii = data;
					var elem = $(this).parent();
					if ( data.success )
					{
						elem.addClass ('ok').removeClass('error');
					}
					else
					{
						jj = elem;
						elem.addClass ('error').removeClass('ok');
						elem.html(elem.html()+ ': '+ data.error);
					}
				}).fail(function(jqXHR, exception){
					elem.addClass ('error').removeClass('ok');
				});
			}
		});
		return false;
	});
	
});
