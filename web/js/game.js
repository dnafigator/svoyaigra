/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var g_Log = null;

$(document).ready(function(){
	function sendCmd(cmd,data,complete)
	{
		 $.ajax({
			 url: 'game/'+cmd,
			 method: 'get',
			 data: data
		 }).done(function(data){
			 if ( null !== complete )
			 {
				 complete(data);
			 }
			 else
			 {
				 location.reload();
			 }
		 }).error(function(data){
			 console.log ( data );
			 alert ( data.responseText );
		 });
	}
	function updateLog ()
	{
		sendCmd('log',null,function(data){
		   var line = '';
		   g_Log = data;
		   for ( i in data ) {
				line += 'Ход ' + i + ': ';
				for ( var j in data [ i ] ) {
					o = data[i][j];
					line += '' + o.name  + ' (' + o.score + ') ';
				}
				line += '<br/>';
		   };
		   $('.game-log').html(line);
			pressButtons ();
		});
	}
	
	function pressButtons ()
	{
//		$('button.add-score').removeClass('btn-success btn-danger btn-warning');
		$('button.add-score').each(function(){
			$(this).addClass ( parseInt($(this).attr ( 'score' )) > 0 ? 'btn-success' : (parseInt($(this).attr ( 'score' )) < 0 ? 'btn-danger' : 'btn-warning'));
		});
		
		var step = $('.current-step').html();
		for ( var j in g_Log [ step ] ) {
			o = g_Log[step][j];
			$('button#'+o.player_id+'.add-score[score="'+parseInt(o.score)+'"]').removeClass('btn-success btn-danger btn-warning');
		}
		
		console.log ( '!!!' );
		score = 10 * ( (step-1) % 5 ) + 10;
		$('.player-in-game>div>.score-buttons>.add-score').parent().addClass ('hidden');
		$('.player-in-game>div>.score-buttons>.add-score[score=0]').parent().removeClass ('hidden');
		$('.player-in-game>div>.score-buttons>.add-score[score='+score+']').parent().removeClass ('hidden');
		$('.player-in-game>div>.score-buttons>.add-score[score=-'+score+']').parent().removeClass ('hidden');
	}
	   
   $('.new-game').on('click', function(){
		if ( confirm ( 'Начать новую игру?') )
		{
			sendCmd('new', null, null);
		}
		return false;
   });
/*   $('.end-game').on('click', function(){
		if ( confirm ( 'Закончить игру? Предыдущие результаты будут сохранены. И их фиг исправишь.') )
		{
			sendCmd('end', null, null);
		}
		return false;
   });
*/ 
   
   $('.next-step').on('click', function(){
        sendCmd('next-step', null, function(obj){
            $('.current-step').html(obj.step);
			pressButtons ();
        });
		return false;
   });
   $('.prev-step').on('click', function(){
        sendCmd('prev-step', null, function(obj){
            $('.current-step').html(obj.step);
			pressButtons ();
        });
		return false;
   });
   $('.add-player').on('click', function(){
        sendCmd('add-player', { player_id: $(this).attr('id') }, function(obj){
            $(".players-in-game>#"+obj.player_id+".player-in-game").removeClass ('hidden');
            $(".player-list>#"+obj.player_id+".player-list-item>.add-player").addClass ('hidden');
            $(".player-list>#"+obj.player_id+".player-list-item>.del-player").removeClass ('hidden');
        });
        return false;
    });
    $('.del-player').on('click', function(){
		if ( confirm ( 'Убрать игрока? Его результаты в текущей игре будут удалены.') )
        sendCmd('del-player', { player_id: $(this).attr('id') }, function(obj){
            $(".players-in-game>#"+obj.player_id+".player-in-game").addClass ('hidden');
            $(".player-list>#"+obj.player_id+".player-list-item>.del-player").addClass ('hidden');
            $(".player-list>#"+obj.player_id+".player-list-item>.add-player").removeClass ('hidden');
            $(".players-in-game>#"+obj.player_id+".player-in-game>.score").html ('0');
        });
        return false;
    });
    
   $('.add-score').on('click', function(){
        sendCmd('add-score', { player_id: $(this).attr('id'), score: $(this).attr('score') }, function(obj){
			console.log ( obj );
            $(".players-in-game>#"+obj.player_id+".player-in-game>.score").html (obj.score);
            updateLog ();
        });
        return false;
   });
   
   $('.player-list-toggle').on('click', function(){
	  if ( $('.player-list').hasClass ( 'hidden' ) ) 
	  {
		  $('.player-list').removeClass ( 'hidden' );
	  }
	  else
	  {
		  $('.player-list').addClass ( 'hidden' );
	  }
		return false;
   });
   
   updateLog ();
});