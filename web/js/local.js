/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function(){
	
	function showCurrent(results)
	{
		$(".player").each(function(ind, item){
			var found = false;
			for ( d in results )
			{
				found |= item.id == results[d].id;
			}
			if ( !found )
			{
				item.remove();
			}
		});

		for ( d in results )
		{
			var nameItem = $("#"+results[d].id+".player>.name");
			if ( !nameItem.length )
			{
				playeritem = $('<span id="'+results[d].id+'" class="player"><span class="name"></span><br/><div class="scoreframe"><div class="score"></div></div></span>');
				$('.players').append ( playeritem );
				nameItem = playeritem.find ( ".name" );
			}
			
			if ( nameItem.html() != results[d].name )
			{
				nameItem.html(results[d].name);
			}
			var score_item = $("#"+results[d].id+".player>.scoreframe>.score");
			if ( score_item.html() !== ""+results[d].score )
			{
				console.log ( 'setting: ' + ""+results[d].score );
				score_item.html(""+results[d].score);
			}
		}
	}
	
	var spp = 0;
	function getResults ()	
	{
        $.ajax({
            url: '/site/get-results',
            method: 'get'
        }).done(function(data){
			$('.pg>span').html ( data.pg );
			if ( null !== data.game)
			{
				$('.game>span').html ( data.game );
				showCurrent(data.results);
				$(".player>.scoreframe").addClass ( 'scoreframecurrent' ).removeClass ( 'scoreframefinal' );
			}
			else if ( null !== data.lastgame)
			{
				$('.game>span').html ( data.lastgame );
				showCurrent(data.final);
				$(".player>.scoreframe").addClass ( 'scoreframefinal' ).removeClass ( 'scoreframecurrent' );
			}
			setTimeout(function(){getResults();}, 3000);
        }).error(function(){
			console.log ( 'reloading (error)!' );
			location.reload();
		});
	}
	
	getResults ();
	
});