/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var timer = 0;
var nFirst = -1;
var nHeight = -1;
var player_lines = [];

$(document).ready(function(){

	function loadPlaygrounds()
	{
        $.ajax({
            url: '/site/get-playground-list',
            method: 'get'
        }).done(function(data){
			$('#chooser option').remove();
			var cnt = 0;
			for ( d in data )
			{
				$('#chooser').append('<option value="'+d+'">'+data[d]+'</option>');
				cnt ++;
			}
			if ( cnt > 1 )
			{
				$('#chooser').removeClass('hidden');
			}
			else
			{
				$('#chooser').addClass('hidden');
			}
			var current = window.location.hash.replace('#', '');
			if ( '' === current)
			{
				$('#chooser option:first').attr('selected','selected');
				window.location.hash = $('#chooser').val();
			}
			else
			{
				$('#chooser option[value="'+current+'"]').attr('selected','selected');
			}
			loadResults();
		});
	}
	
	function loadResults()
	{
        $.ajax({
            url: '/site/get-all-results',
            method: 'get',
			data: { pg: window.location.hash.replace('#', '') }
        }).done(function(data){
			var all_lines = [];
			$('.player-line').each(function(index, elem){
				all_lines.push ( parseInt ( elem.id ) );
			});
			player_lines = [];
			for ( var id in data )
			{
				//console.log ( data [ id ] );
				player_id = data [ id ].id;
				var player_info = $("#"+player_id+".player-line");
				if ( 0 === player_info.size() )
				{
					player_info = $('<div class="player-line" place="" id="'+player_id+'"><span class="place"></span><span class="name"></span><span class="score"></span></div>');
					$('.results').append ( player_info );
				}
				player_lines.push(player_info);
				if ( (ind = all_lines.indexOf ( player_id )) !== -1 )
				{
					all_lines.splice ( ind, 1 );
				}
				var place = player_info.find(".place");
				if ( data [ id ].place !== place.html() )
				{
					place.html(data [ id ].place);
				}
				var name = player_info.find(".name");
				if ( data [ id ].name !== name.html() )
				{
					name.html(data [ id ].name);
				}
				var score = player_info.find(".score");
				if ( data [ id ].score !== score.html() )
				{
					score.html(data [ id ].score);
				}
				if ( data [ id ].place !== player_info.attr('place') )
				{
					player_info.attr('place', data [ id ].place );
				}
			}
			for ( id in all_lines )
			{
				$("#"+all_lines[id]+".player-line").remove();
			}
			
			if ( -1 === nFirst )
			{
				nFirst = $('.player-line:first').position().top;
				nHeight = $('.player-line:first').height() + 10;
			}
			if ( data.length != $('#places').attr ( 'count' ) )
			{
				var styles = [];
				for ( i = 1; i <= data.length; i ++ )
				{
					styles.push (
						'.player-line[place="'+ i + '"] {top:'+(nFirst + (i-1)*nHeight)+'px;}'
					)
				}
				$('#places').html('<style>'+styles.join('\n')+'</style>').attr ( 'count', styles.length );
			}
			
			if ( 0 !== timer )
			{
				clearTimeout ( timer );
				timer = 0;
			}
		
			timer = setTimeout ( function(){loadResults();}, '3000' );
		}).error(function(){
			//location.reload();
		});
	}
	
	
	loadPlaygrounds();
	
	$('#chooser').on ( 'change', function(){
		window.location.hash = $('#chooser').val();
		loadResults();
	});
	
	
});