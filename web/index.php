<?php

// comment out the following two lines when deployed to production
//defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'prod');

require(__DIR__ . '/../vendor/autoload.php');
require(__DIR__ . '/../vendor/yiisoft/yii2/Yii.php');

$global_config = require(__DIR__ . '/../config/web.php');
$env_config = require(__DIR__ . '/../config/'.YII_ENV.'.php');
$config = array_merge_recursive ($global_config, $env_config);


(new yii\web\Application($config))->run();
