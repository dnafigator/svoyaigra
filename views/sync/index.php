<?php
use yii\helpers\Html;

$this->title = 'Синхронизация';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('js/sync.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile('/css/sync.css');


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

echo \yii\helpers\BaseHtml::beginForm('', 'POST', ['id' => 'gamesform']);
echo Html::checkboxList('games', '', $games, ['separator'=>'<span class="errtext"></span><br/>'."\n"] );

echo yii\helpers\Html::submitButton('Синхронизировать');
echo \yii\helpers\BaseHtml::endForm();
