<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
	<link rel="icon" type="image/png" href="/img/favicon.png">
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <div class="container">
		<div class="pull-right breadcrumb">
		<?php if ( \yii::$app->user->isGuest ) 
		{
			echo \yii\helpers\Html::a( 'Вход', '/site/login' );
		}
		else
		{
			echo \yii::$app->user->identity->name.' '.\yii\helpers\Html::a( 'Выход', '/site/logout' );
		}?>
		</div>
        <?= Breadcrumbs::widget([
			'homeLink' => [ 'label' => 'Админка', 'url' => '/admin'],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
	        ]) ?>
        <?= $content ?>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
