<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игроки';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="player-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
	    <?= $this->render('_form', [
			'model' => $model,
		]) ?>
        <!--?= Html::a('Create Player', ['create'], ['class' => 'btn btn-success']) ?-->
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider, 
        'columns' => [
            'id',
            'name',
            [
				'class' => 'yii\grid\ActionColumn',
				 'template'=>'{update} {delete}',
			],
        ],
    ]); ?>

</div>
