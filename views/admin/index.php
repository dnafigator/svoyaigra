<?php

/* @var $this yii\web\View */


$this->title = 'Своя игра / Админ';

use yii\helpers\Html;
?>
<?php echo Html::a('Игроки','/player');?>
<br/>
<?php echo Html::a('Игра','/game');?>
<!--br/>
<?php echo Html::a('Результаты игры','/site/simple-local');?>-->
<br/>
<?php echo Html::a('Общие результаты','/site/simple-global');?>
<br/>
<hr/>
<?php echo Html::a('Сменить площадку','/site/change-pg');?>
<br/>

<?php echo Html::a('Синхронизация результатов','/sync/');?>
<br/>
<hr/>
<?php echo Html::a('Табло локальное','/site/local', [ 'target' => '_blank' ]);?>
<br/>
<?php echo Html::a('Табло общее','/site/global', [ 'target' => '_blank' ]);?>
<br/>
