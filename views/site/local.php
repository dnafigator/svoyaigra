<?php
use yii\helpers\Html;
use yii\helpers\BaseHtml;

$this->title = 'Текущая игра';

$this->registerJsFile('js/local.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile('/css/local.css');
?>
<div class="pg">Площадка <span></span></div>
<div class="game">Игра <span></span></div>
<div class="players"></div>
<div class="totalresults"></div>
