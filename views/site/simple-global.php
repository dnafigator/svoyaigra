<?php


use yii\helpers\Html;
use yii\helpers\BaseHtml;

$this->title = 'Общие результаты';
$this->params['breadcrumbs'][] = $this->title;

/* @var $this yii\web\View */
?>
<div class="all-games">
<?php
$prev_game = 1;
$prev_pg = 0;
foreach ( $res as $pgs )
{
	foreach ( $pgs as $games )
	{
		foreach ( $games as $r )
		{?>
		<div class="row">
			<div>Площадка <?php echo $r['pg']?></div>
			<div>Игра <?php echo $r['game_id']?></div>
			<div><?php echo $r['name']?></div>
			<div><?php echo $r['score']?></div>
		</div><?php
		}
		echo '<div class="row"><div class="separator"><hr/></div></div>';
	}
}?>
</div>