<?php


use yii\helpers\Html;
use yii\helpers\BaseHtml;

/* @var $this yii\web\View */

$this->registerCssFile('/css/index.css');
$this->registerJsFile('js/index.js', ['depends' => 'yii\web\JqueryAsset'] );

$this->title = 'Своя игра';

?>
<div id="main">
</div>
