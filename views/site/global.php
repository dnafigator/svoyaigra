<?php


use yii\helpers\Html;
use yii\helpers\BaseHtml;

/* @var $this yii\web\View */

$this->registerJsFile('js/global.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile('/css/global.css');

$this->title = 'Своя игра';
echo Html::dropDownList('pg', '', array(), ['id' => 'chooser', 'class' => 'hidden' ] );
?>


<div class="results"></div>
<div id="places" count="0">
</div>