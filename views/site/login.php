<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;

$this->title = 'Вход';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if( isset ( $error ) ) 
	{
		echo yii\helpers\Html::label($error);
	}
	?>
    <?php echo yii\helpers\Html::beginForm(); ?>

        <?php echo yii\helpers\Html::textInput('login', isset ( $login ) ? $login : '') ?>
	
        <?php echo yii\helpers\Html::passwordInput('password') ?>
	
        <?php echo yii\helpers\Html::hiddenInput('goback', $goback) ?>

        <?php echo yii\helpers\Html::submitButton('Войти') ?>
	
    <?php echo yii\helpers\Html::endForm() ?>

</div>
