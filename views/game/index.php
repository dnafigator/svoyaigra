<?php

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Игра';
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('js/game.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile('/css/game.css');
?>
<div class="player-index">
<a class="new-game" href="#">Новая</a>
<a class="select-game" href="/game/select">Выбрать</a>
	<div class="title">
		Площадка <?php echo $parm['gamestate']['playground'];?>
	</div>
	<div class="subtitle">
		<?php 
		if ( isset ( $parm['game']['id'] ) )
		{?>
		Игра <?php echo $parm['game']['id'];?> начата <?php echo $parm['game']['create_date'];?>
		<?php }else{?>
			Игры нет
		<?php }?>
	</div>
	


<span class="player-select"><a href="#" class="player-list-toggle">Игроки</a></span>
<span class="player-list hidden">
<?php foreach ( $parm['players'] as $id => $player )
{
    $in_game = $player['in_game'];
    $addLink = '<a class="player-list-name add-player'.($in_game?' hidden':'').'" href="#" id="'.$id.'">Добавить '.$player['name'].'</a>';
    $delLink = '<a class="player-list-name del-player'.($in_game?'':' hidden').'" href="#" id="'.$id.'">Убрать '.$player['name'].' </a>';
    echo '<span class="player-list-item" id="'.$id.'">'.$addLink.$delLink.'</span>'."<br/>\n";
}?>
</span>

<div class="step-info">
<?php echo \yii\bootstrap\Button::widget([
	'label' => '<<',
	'options' => [
		'class' => 'prev-step'
		]
	]);?>
	<span class="current-step-text">Ход <span class="current-step"><?php echo isset ( $parm['gamestate']['current_step'] ) ? $parm['gamestate']['current_step'] : '-'; ?></span></span>
<?php echo \yii\bootstrap\Button::widget([
	'label' => '>>',
	'options' => [
		'class' => 'next-step'
		]
	]);?>
</div>

<div class="players-in-game">
<?php function scoreLink ( $player_id, $score )
{
	
	return \yii\bootstrap\Button::widget([
	'label' => ''.$score,
	'options' => [
		'class' => 'add-score '.(($score===0)?'btn-warning':(($score>0)?'btn-success':'btn-danger')),
		'id' => $player_id,
		'score' => ''.$score
		]
	]);	
}

foreach ( $parm['players'] as $id => $player )
{
    $in_game = $player['in_game'];
    echo '<div class="player-in-game'.($in_game?'':' hidden').'" id="'.$id.'"><span class="player-name">'.$player['name'].'</span> <span class="score">'.$player['score'].'</span>';
	echo '<div>';
	echo '<span class="score-buttons">'.scoreLink ( $id, 0 ).'</span>';
    foreach ( [10, 20, 30, 40, 50 ] as $score )
    {
        echo '<span class="score-buttons">'.scoreLink ( $id, -$score ).scoreLink ( $id, $score ).'</span>';
    }
    echo '</div></div>'."\n";
}
?>
</div>

<div class="end-game-panel"><a class="end-game" href="/game/end">Закончить игру</a></div>


<div class="game-log"></div>

</div>
