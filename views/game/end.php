<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Конец игры';
$this->params['breadcrumbs'][] = [ 'url' => '/game', 'label' => 'Игра' ];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('js/endgame.js', ['depends' => 'yii\web\JqueryAsset'] );
$this->registerCssFile('/css/endgame.css');
?>
<div class="player-index">
	<div class="title">
		Площадка <?php echo $pg;?>
	</div>
	<div class="subtitle">
		Игра <?php echo $game['id'];?> начата <?php echo $game['create_date'];?>
	</div>
	
	
	<div id="results">
		<?php
		echo \yii\helpers\BaseHtml::beginForm('', 'POST');
		echo yii\helpers\Html::input('hidden', 'end', $end );
		foreach ( $players as $id => $player )
		{
			echo '<div class="player-line" id="'.$id.'" >';
			echo '<span class="name">'.$player['name'].'</span><span class="score">'.$player['score'].'</span><span class="formula">'.$player['formula'].'</span><span class="rating">';
			echo yii\helpers\Html::input('hidden', 'player['.$id.'][id]', $id );
			echo yii\helpers\Html::input('hidden', 'player['.$id.'][name]', $player['name'] );
			echo yii\helpers\Html::input('text', 'player['.$id.'][rating]', $player['rating'] );
			echo '</span></div>';
		}
		echo '<div class="save">'.yii\helpers\Html::submitButton('Сохранить').'</div>';
		echo \yii\helpers\BaseHtml::endForm();
		?>
	</div>
	<hr/>
	<div id="gdocs">
		<ol>
			<li>Нажать внутри серой области.
			<li>Нажать Ctrl+C.
			<li>Перейти в документ «Круговой этап».
			<li>Выделить левыую верхнюю ячейку нужного боя.
			<li>Нажать Ctrl+Shift+V.
		</ol>
		<textarea id="gdocs-text" rows="4" cols="30"><?php
		$row = 0;
		preg_match('/ ([0-9][0-9]:[0-9][0-9]):/', $end, $m);
		$end_time = $m[1]; 
		foreach ( $players as $id => $player )
		{
			echo $player['name']."\t".$end_time."\t".$player['score'];
			if ( $row++ < 3 ) echo "\n";
		}
?></textarea>
	</div>
</div>
