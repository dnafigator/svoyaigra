<?php
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Выбор игры';
$this->params['breadcrumbs'][] = [ 'url' => '/game', 'label' => 'Игра' ];
$this->params['breadcrumbs'][] = $this->title;

//$this->registerJsFile('js/select.js', ['depends' => 'yii\web\JqueryAsset'] );
//$this->registerCssFile('/css/select.css');
?>
<div class="player-index">
	<div class="title">
		Площадка <?php echo $pg;?>
	</div>
	<?php
	foreach ( $games as $game ){?>
	<a href="?game_id=<?php echo $game['id']?>">Игра <?php echo $game['id']?>: <?php echo $game['start'];if ($game['end'] !== null ) echo ' — '.$game['end'];?></a><br/>
	<?php }?>
	<a href="/game">Отменить</a><br/>
</div>
