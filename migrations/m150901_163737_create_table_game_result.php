<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_163737_create_table_game_result extends Migration
{
    public function up()
    {
        $this->createTable('svoyai_game_result', [
            'id' => 'pk',
            'playground' => 'int(11) NOT NULL',
            'game_id' => 'int(11) NOT NULL',
            'create_date' => 'DATETIME NOT NULL',
            'end_date' => 'datetime NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			'player_id' => 'int(11) NOT NULL',
			'name' => 'varchar(100) NOT NULL',
			'rating' => 'DECIMAL(10,3) NOT NULL',
        ], 'ENGINE=InnoDB CHARSET=utf8');

    }

    public function down()
    {
       $this->dropTable('svoyai_game_result');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
