<?php

use yii\db\Schema;
use yii\db\Migration;

class m150806_205825_init_database extends Migration
{
    public function up()
    {
        $this->createTable('svoyai_game', [
            'id' => 'pk',
            'playground' => 'int(11) NOT NULL',
            'create_date' => 'DATETIME NOT NULL',
            'end_date' => 'datetime',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'

        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('svoyai_player', [
            'id' => 'pk',
            'name' => 'varchar(100) NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
		], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('svoyai_game_player', [
            'id' => 'pk',
            'game_id' => 'int(11) NOT NULL',
            'player_id' => 'int(11) NOT NULL',
            'score' => 'int(11) NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'

        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('svoyai_results', [
            'id' => 'pk',
            'game_id' => 'int(11) NOT NULL',
            'player_id' => 'int(11) NOT NULL',
            'step' => 'int(11) NOT NULL',
            'score' => 'int(11) NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'

        ], 'ENGINE=InnoDB CHARSET=utf8');

        $this->createTable('svoyai_gamestate', [
            'id' => 'pk',
            'playground' => 'int(11) NOT NULL',
            'playground_secret' => 'varchar(100) NOT NULL',
            'current_game_id' => 'int(11)',
            'current_step' => 'int(11)',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'

        ], 'ENGINE=InnoDB CHARSET=utf8');
		
        $this->createTable('svoyai_overall_display', [
            'id' => 'pk',
            'playground' => 'int(11) NOT NULL',
            'game_id' => 'int(11) NOT NULL',
            'create_date' => 'datetime NOT NULL',
            'end_date' => 'datetime',
            'player_id' => 'int(11) NOT NULL',
            'player_name' => 'varchar(100) NOT NULL',
            'player_score' => 'int(11) NOT NULL',
        ], 'ENGINE=InnoDB CHARSET=utf8');
		
        $this->createTable('svoyai_user', [
            'id' => 'pk',
            'name' => 'varchar(100) NOT NULL',
            'email' => 'varchar(100) NOT NULL',
            'pwdhash' => 'varchar(100) NOT NULL',
            'salt' => 'varchar(100) NOT NULL',
			'auth_key' => 'varchar(100) NOT NULL',
            'modify_date' => 'TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'
        ], 'ENGINE=InnoDB CHARSET=utf8');
    }

    public function down()
    {
        $this->dropTable('svoyai_game');
        $this->dropTable('svoyai_player');
        $this->dropTable('svoyai_game_player');
        $this->dropTable('svoyai_results');
        $this->dropTable('svoyai_gamestate');
        $this->dropTable('svoyai_overall_display');
        $this->dropTable('svoyai_user');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
