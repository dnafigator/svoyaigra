<?php

use yii\db\Schema;
use yii\db\Migration;

class m150915_132906_alter_gamestate_add_last_game extends Migration
{
    public function up()
    {
		$this->addColumn('svoyai_gamestate', 'last_game', 'int(11)');
    }

    public function down()
    {
		$this->dropColumn('svoyai_gamestate', 'last_game');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
