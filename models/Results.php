<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%results}}".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $player_id
 * @property integer $step
 * @property integer $score
 * @property string $modify_date
 */
class Results extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%results}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'player_id', 'step', 'score'], 'required'],
            [['game_id', 'player_id', 'step', 'score'], 'integer'],
            [['modify_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'player_id' => 'Player ID',
            'step' => 'Step',
            'score' => 'Score',
            'modify_date' => 'Modify Date',
        ];
    }
}
