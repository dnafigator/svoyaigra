<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%game}}".
 *
 * @property integer $id
 * @property integer $playground
 * @property string $create_date
 * @property string $end_date
 * @property string $modify_date
 */
class Game extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playground'], 'required'],
            [['playground'], 'integer'],
            [['create_date', 'end_date', 'modify_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playground' => 'Playground',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'modify_date' => 'Modify Date',
        ];
    }
}
