<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%game_player}}".
 *
 * @property integer $id
 * @property integer $game_id
 * @property integer $player_id
 * @property integer $score
 * @property string $modify_date
 */
class GamePlayer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_player}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['game_id', 'player_id', 'score'], 'required'],
            [['game_id', 'player_id', 'score'], 'integer'],
            [['modify_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'game_id' => 'Game ID',
            'player_id' => 'Player ID',
            'score' => 'Score',
            'modify_date' => 'Modify Date',
        ];
    }
}
