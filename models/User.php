<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $pwdhash
 * @property string $salt
 * @property string $auth_key
 * @property string $modify_date
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'email', 'pwdhash', 'salt'], 'required'],
            [['modify_date'], 'safe'],
            [['name', 'email', 'pwdhash', 'salt'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'pwdhash' => 'Pwdhash',
            'salt' => 'Salt',
            'modify_date' => 'Modify Date',
			'auth_key' => 'Auth Key',
        ];
    }

	public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = \Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }	
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    public static function findIdentityByAccessToken($token, $type = null)
	{
		return static::findOne(['access_token' => $token]);
	}
    public function getId()
	{
		return $this->id;
	}
    public function getAuthKey()
	{
		 return $this->auth_key;
	}
	
    public function validateAuthKey($authKey)
	{
		return $this->getAuthKey() === $authKey;
	}
	
    public function validatePassword($password)
    {
        return $this->pwdhash === md5 ( md5 ( $password ).$this->salt );
    }
	
}
