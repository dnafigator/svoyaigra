<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%game_result}}".
 *
 * @property integer $id
 * @property integer $playground
 * @property integer $game_id
 * @property string $create_date
 * @property string $end_date
 * @property string $modify_date
 * @property string $name
 * @property string $rating
 */
class GameResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%game_result}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playground', 'game_id', 'create_date', 'end_date', 'name', 'rating'], 'required'],
            [['playground', 'game_id'], 'integer'],
            [['create_date', 'end_date', 'modify_date'], 'safe'],
            [['rating'], 'number'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playground' => 'Playground',
            'game_id' => 'Game ID',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'modify_date' => 'Modify Date',
            'name' => 'Name',
            'rating' => 'Rating',
        ];
    }
}
