<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%overall_display}}".
 *
 * @property integer $id
 * @property integer $playground
 * @property integer $game_id
 * @property string $create_date
 * @property string $end_date
 * @property integer $player_id
 * @property string $player_name
 * @property integer $player_score
 */
class OverallDisplay extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%overall_display}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playground', 'game_id', 'create_date', 'player_id', 'player_name', 'player_score'], 'required'],
            [['playground', 'game_id', 'player_id', 'player_score'], 'integer'],
            [['create_date', 'end_date'], 'safe'],
            [['player_name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playground' => 'Playground',
            'game_id' => 'Game ID',
            'create_date' => 'Create Date',
            'end_date' => 'End Date',
            'player_id' => 'Player ID',
            'player_name' => 'Player Name',
            'player_score' => 'Player Score',
        ];
    }
}
