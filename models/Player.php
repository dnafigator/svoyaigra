<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%player}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $modify_date
 */
class Player extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%player}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['modify_date'], 'safe'],
            [['name'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'modify_date' => 'Дата последнего изменения',
        ];
    }
}
