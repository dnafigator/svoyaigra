<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%gamestate}}".
 *
 * @property integer $id
 * @property integer $playground
 * @property string $playground_secret
 * @property integer $current_game_id
 * @property integer $current_step
 * @property string $modify_date
 * @property integer $last_game
 */
class Gamestate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%gamestate}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['playground', 'playground_secret'], 'required'],
            [['playground', 'current_game_id', 'current_step', 'last_game'], 'integer'],
            [['modify_date'], 'safe'],
            [['playground_secret'], 'string', 'max' => 100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'playground' => 'Playground',
            'playground_secret' => 'Playground Secret',
            'current_game_id' => 'Current Game ID',
            'current_step' => 'Current Step',
            'modify_date' => 'Modify Date',
            'last_game' => 'Last Game',
        ];
    }
}
