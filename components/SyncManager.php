<?php

namespace app\components;

class SyncManager extends \yii\base\Component
{
    var $cfg = null;
    
    public function hash( $gs, $secret )
    {
        return md5 ( $gs->playground.$gs->current_game_id.$gs->current_step.$secret);
    }

    public function init()
    {
        $this->cfg = require ( __DIR__.'/../config/playgrounds.php');
    }
    
    public function checkHash( $gs, $hash )
    {
        return $hash == $this->hash( $gs, $this->cfg['secrets'][ $gs->playground ] );
    }
    
    
    public function push()
    {
        $this->init();
        $gamestate = \app\models\Gamestate::find()->one();
        if ( null === $gamestate )
        {
            throw new yii\console\Exception ( 'Площадка не инициализирована' );
        }
        
        
        $gps = \app\models\GamePlayer::find()->where ( ['game_id' => $gamestate->current_game_id] )->all();
        $results = [];
        foreach ( $gps as $gp )
        {
            $p = \app\models\Player::findOne($gp->player_id);
            $results[] = [ 'name' => $p->name, 'score' => $gp->score ];
        }
        $ret = [
            'gamestate' => $gamestate->toArray(['playground','current_game_id','current_step']),
			'gameinfo' => \app\models\Game::findOne($gamestate->current_game_id)->toArray(),
            'hash' => $this->hash($gamestate,$gamestate->playground_secret),
            'results' => $results
        ];
		
		$client = new \GuzzleHttp\Client(['base_uri' => $this->cfg['central'], 'timeout' => 2000 ]);
        $response = $client->post ( '/sync/receive', ['body' => json_encode($ret)] );
        echo $response->getBody();		
		//echo json_encode ( $ret );
        return 0;
    }
    
    public function receive($data)
    {
        $this->init();
        $json = json_decode ( $data );
		print_r ( $json );
		die;
        if ( $this->checkHash( $json->gamestate, $json->hash ) )
        {
			\app\models\OverallDisplay::deleteAll(['playground' => $json->gamestate->playground, 'game_id' => $json->gamestate->current_game_id ]);
			foreach ( $json->results as $result )
			{
				$rec = new \app\models\OverallDisplay ();
				$rec->playground = $json->gamestate->playground;
				$rec->game_id = $json->gamestate->current_game_id;
				$rec->create_date = $json->gamestate->create_date;
				$rec->end_date = $json->gamestate->end_date;
				$rec->player_id = $result->player_id;
				$rec->player_name = $result->name;
				$rec->player_score = $result->score;
				$rec->save();
			}
			
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return ['success' => true ];
        }
		else {
            throw new \yii\web\HttpException ( 403, 'Вы кем-то притворяетесь' );
		}
    }

}
