<?php
namespace app\controllers;
use Yii;
use yii\web\Controller;
class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
		$this->layout = 'empty';
		return $this->render('index');
    }
	
    public function actionGlobal()
    {
		$this->layout = 'results';
        return $this->render('global');
    }
    public function actionSimpleGlobal()
    {
		$res = \app\models\GameResult::find()->select(['playground', 'player_id', 'name', 'game_id', 'rating'])->orderBy('playground asc, game_id asc, rating desc')->all();
		$i = 0;
		$ret = [];
		foreach ( $res as $id => $r )
		{
			$ret [$r->playground][$r->game_id][ $id ] = [
				'pg' => $r->playground,
				'id' => $r->player_id,
				'name' => $r->name,
				'game_id' => $r->game_id,
				'score' => $r->rating,
				];
		}
		
        return $this->render('simple-global', ['res' => $ret]);
    }
    public function actionChangePg()
	{
		\app\models\Gamestate::deleteAll();
		echo 'ok';
	}
	
    public function actionLocal()
    {
		$this->layout = 'results';
        return $this->render('local');
    }
	
    public function actionTotalLocal()
	{
		$users = \app\models\Player::find()->all();
		$us = [];
		foreach ( $users as $user )
		{
			$us[$user->id] = $user->name;
		}
		
        $ress = \app\models\Results::find()->all();
		$results = [];
		echo 'Игра;Ход;Игрок;Имя;Баллы'."\n";
		foreach ( $ress as $res )
		{
			echo $res->game_id.';'.
					$res->step.';'.
					$res->player_id.';'.
					$us[$res->player_id].';'.
					$res->score."\n";

			$results [] = [
				'game' => $res->game_id,
				'step' => $res->step,
				'name' => $us[$res->player_id],
				'score' => $res->score
			];
		}
	}
			
    public function actionSimpleLocal()
    {
        $ress = \app\models\Results::find()->all();
		foreach ( $ress as $res )
		{
			$results [ $res->game_id ][ $res->player_id ][ $res->step ] = $res->score;
		}
		
        return $this->render('simple-local', ['results' => $results ]);
    }
	
    public function actionGetResults()
	{
		$gs = \app\models\Gamestate::find()->one();
		$results = [];
		$final = [];
		if ( null !== $gs && null !== $gs->current_game_id )
		{
			$gps = \app\models\GamePlayer::findAll(['game_id' => $gs->current_game_id]);
			foreach ( $gps as $gp )
			{
				$p = \app\models\Player::findOne($gp->player_id);
				$results[] = [
					'id' => $p->id,
					'name' => $p->name,
					'score' => $gp->score
					];
			}
		}
		if ( null !== $gs->last_game )
		{
			$res = \app\models\GameResult::find()->select(['player_id', 'name', 'SUM(rating) as rating'])->where(['playground' => $gs->playground, 'game_id' => $gs->last_game])->groupBy('player_id')->orderBy('rating DESC')->all();
			$i = 0;
			foreach ( $res as $r )
			{
				$final [] = [
					'id' => $r->player_id,
					'name' => $r->name,
					'score' => $r->rating,
					];
			}
			
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return [
			'pg' => $gs->playground,
			'game' => $gs->current_game_id,
			'lastgame' => $gs->last_game,
			'results' => $results,
			'final' => $final,
			];
	}
	
	public function actionGetAllResults($pg)
	{
		$res = \app\models\GameResult::find()->select(['player_id', 'name', 'SUM(rating) as rating'])->where(['playground' => $pg])->groupBy('player_id')->orderBy('rating DESC')->all();
		$i = 0;
		foreach ( $res as $id => $r )
		{
			$ret [ $id ] = [
				'id' => $r->player_id,
				'name' => $r->name,
				'score' => $r->rating,
				'place' => ++ $i,
				];
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $ret;
	}
	
    public function actionGetPlaygroundList()
	{
		$pgs = \app\models\GameResult::find()->select('playground')->distinct()->asArray ()->column();
		$ret = [];
		foreach ( $pgs as $pg )
		{
			switch ( $pg )
			{
				case 5:
					$ret [ $pg ] = 'Золотой зал';
					break;
				case 6:
					$ret [ $pg ] = 'Серебряный зал';
					break;
				case 7:
					$ret [ $pg ] = 'Красный зал Valhalla';
					break;
				case 8:
					$ret [ $pg ] = 'Черный зал Valhalla';
					break;
				case 9:
					$ret [ $pg ] = 'Финал Кубка Вальгаллы';
					break;
				case 10:
					$ret [ $pg ] = 'Финал';
					break;
				default:
					$ret [ $pg ] = 'Площадка '.$pg;
			}
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $ret;
	}
	
    public function actionAbout()
    {
        return $this->render('about');
    }
	
    public function actionLogin()
	{
		if ( !\yii::$app->request->isPost )
		{
			return $this->render('login', [ 'goback' => \yii::$app->request->referrer ]);
		}
		$login = \yii::$app->request->post('login');
		$pwd = \yii::$app->request->post('password');
		$goback = \yii::$app->request->post('goback');
		
		$identity = \app\models\User::findOne(['name' => $login]);
		if ( $identity )
		{
			if ( $identity->validatePassword ( $pwd ) )
			{
				Yii::$app->user->login($identity);
				if ( 0 !== strstr ( $goback, '/login' ) )
				{
					$this->redirect('/admin');
				}
				else {
					$this->redirect($goback);
				}
				return;
			}
		}
		{
			return $this->render('login', ['login' => $login, 'error' => 'error', 'goback' => \yii::$app->request->post('goback') ]);
		}
	}
	
    public function actionLogout()
	{
		Yii::$app->user->logout();
		$this->redirect('/admin');
	}
}
