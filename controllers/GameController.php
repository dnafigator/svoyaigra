<?php

namespace app\controllers;

use yii\web\Controller;
use \app\models;

class GameController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
//				'only' => ['index'],
				'rules' => [
					// allow authenticated users
					[
						'allow' => true,
						'roles' => ['@'],
					],
					// everything else is denied
				],
			],            
		];
	}	
    
    var $gamestate = null;
    public function gameID()
    {
		if ( null === $this->gamestate )
		{
            $this->gamestate = \app\models\Gamestate::find()->one();
            if ( null === $this->gamestate )
            {
                //init playground
                $this->gamestate = new \app\models\Gamestate();
                $this->gamestate->playground = (int)file_get_contents( __DIR__.'/../config/playground');
                $cfg = require ( __DIR__.'/../config/playgrounds.php');
                $playgrounds = $cfg['secrets'];
                $this->gamestate->playground_secret = $playgrounds [ $this->gamestate->playground ];
                $this->gamestate->save();
            }
        }
        return $this->gamestate->current_game_id;
    }

	private function getPlaces( $g )
	{
		$places = [];
		$first = 0;
		while ( $first < sizeof ( $g ) )
		{
			$last = $first;
//			while ( $last < sizeof ( $g ) && $g[$last] == $g[$first])
			while ( $last < sizeof ( $g ) && $g[$last]->score == $g[$first]->score )
				$last ++;
			for ( $pos = $first; $pos < $last; $pos ++ )
			{
				$places[$pos]['start_place'] = $first+1;
				$places[$pos]['end_place'] = $last;
			}
			$first = $last;
		}
		return $places; 
	}
	private function getRatings( $g )
	{
		$p = $this->getPlaces ( $g );
		$ratings = [ 0, 4, 2, 1, 0 ];
		$r = [];
		for ( $i = 0; $i < sizeof ( $p ); $i ++ )
		{
			$place = $p[$i]['start_place'] == $p[$i]['end_place'] ? $p[$i]['start_place']. ' место' : $p[$i]['start_place'].'-'.$p[$i]['end_place'].' места';
			$r[$i] = [
				'places' => $place,
				'rating' => round ( ( $ratings[$p[$i]['end_place']] + ( $ratings[$p[$i]['start_place']] - $ratings[$p[$i]['end_place']] ) / ($p[$i]['end_place'] - $p[$i]['start_place'] + 1) ), 3 )
			];
		}
		return $r;
	}

	public function aa($a)
	{
		$r = $this->getRatings($a);
		for ( $i = 0; $i < sizeof ( $a ); $i++ )
		{
			echo $a[$i].' -> '.$r[$i]."<br/>\n";
		}
		echo '<hr/>';
	}
	
	public function actionIndex()
    {
/*		echo '<pre>';
		$this->aa([4,3,2,1]);
		$this->aa([4,4,2,1]);
		$this->aa([4,4,4,1]);
		$this->aa([4,4,4,4]);
		$this->aa([4,3,3,1]);
		$this->aa([4,3,3,3]);
		$this->aa([4,3,2,2]);
		$this->aa([4,4,2,2]);
		echo '</pre>';
		
		die;
*/		
        $this->gameID();
        
        $players = [];
		$game = null;
        if ( null !== $this->gamestate->current_game_id )
        {
            $game = models\Game::findOne($this->gamestate->current_game_id);
            if ( null !== $game )
            {
                $gameplayers = models\GamePlayer::find()->where ( ['game_id' => $game->id] )->all();
                $allplayers = models\Player::find()->all();
                foreach ( $allplayers as $player )
                {
                    $players[$player->id] = models\Player::findOne($player->id)->toArray();
                    $players[$player->id]['in_game'] = false;
                    $players[$player->id]['score'] = 0;
                }
                foreach ( $gameplayers as $player )
                {
                    $players[$player->player_id]['in_game'] = true;
                    $players[$player->player_id]['score'] = $player->score;
                }
            }
        }
        
        $parm = [
          'gamestate'  => $this->gamestate ? $this->gamestate->toArray(['playground','current_game_id','current_step','modify_date']) : [],
          'game'  => $game ? $game->toArray() : [],
          'players'  => $players
        ];
        
        return $this->render('index', ['parm' => $parm ] );
    }

    public function actionEnd()
    {
		$this->gameID();
		if ( null === $this->gamestate->current_game_id )
		{
			return $this->redirect('/game');
		}		
		$game = models\Game::findOne($this->gamestate->current_game_id);
		if ( null === $game )
		{
			return $this->redirect('/game');
		}
		
		if ( \yii::$app->request->isPost)
		{
			$game->end_date = \yii::$app->request->post('end');
			models\GameResult::deleteAll(['game_id' => $this->gamestate->current_game_id, 'playground' => $this->gamestate->playground]);
			foreach ( \yii::$app->request->post('player') as $p )
			{
				$gr = new models\GameResult();
				$gr->playground = $this->gamestate->playground;
				$gr->game_id = $this->gamestate->current_game_id;
				$gr->create_date = $game->create_date;
				$gr->end_date = $game->end_date;
				$gr->player_id = $p [ 'id' ];
				$gr->name = $p [ 'name' ];
				$gr->rating = $p [ 'rating' ];
				$gr->save ();
			}
			$this->gamestate->current_step = 0;
			$this->gamestate->last_game = $this->gamestate->current_game_id;
			$this->gamestate->current_game_id = null; 
			$game->save();
			$this->gamestate->save();
			\Yii::$app->runAction('sync/push', ['game_id' => $game->id]);
		}
		else
		{
			$players = [];
			$gameplayers = models\GamePlayer::find()->where ( ['game_id' => $game->id] )->orderBy('score DESC')->all();
			
			$rs = $this->getRatings( $gameplayers );
		
			$i = 0;
			foreach ( $gameplayers as $id => $gameplayer )
			{
				$player = models\Player::findOne($gameplayer->player_id);
				$players[$gameplayer->player_id] = ['name' => $player->name, 'score' => $gameplayer->score, 'rating' => $rs [$id]['rating'] + $gameplayer->score / 1000, 'formula' => '= '.$rs [$id]['rating'].' ('.$rs [$id]['places'].') + 0.001 * '.$gameplayer->score ];
			}
			return $this->render('end', ['players' => $players, 'pg' => $this->gamestate->playground, 'game' => $game, 'end' => date ( 'Y-m-d H:i:s' ) ] );
		}
		return $this->redirect('/game');
	}
    public function actionSelect($game_id = 0)
	{
		$this->gameID();
		if ( 0 !== $game_id )
		{
			$this->gamestate->current_game_id = $game_id;
			$this->gamestate->current_step = 1;
			$this->gamestate->save();
			return $this->redirect('/game' );
		}
		else
		{
			$allgames = models\Game::find()->all();
			$games = [];
			foreach ( $allgames as $game )
			{
				$games[] = ['id' => $game->id, 'start' => $game->create_date, 'end' => $game->end_date ];
			}
			return $this->render('select', ['games' => $games, 'pg' => $this->gamestate->playground] );
		}
	}
	
    public function actionNew()
    {
        $oldgame = models\Game::findOne($this->gameID());
        if ( null !== $oldgame )
        {
            $oldgame->end_date = date ( 'Y-m-d H:i:s' );
            $oldgame->save();
			//TODO:: push old game
			
        }
        
        $newgame = new \app\models\Game();
        $newgame->playground = $this->gamestate->playground;
		$newgame->create_date = date ( 'Y-m-d H:i:s' );
        $newgame->save();
        $curgame = \app\models\Game::findOne($newgame->id);
        
        $this->gamestate->current_game_id = $curgame->id; 
        $this->gamestate->current_step = 1;
        $this->gamestate->save ();
        http_response_code(200);
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return $curgame->toArray();
    }

    public function actionAddPlayer($player_id)
    {
        $game_id = $this->gameID();
        if (null === \app\models\GamePlayer::find()->where ( ['player_id' => $player_id, 'game_id' => $game_id ])->one() )
        {
			$cnt = \app\models\GamePlayer::find()->where ( ['game_id' => $game_id ])->count();
			if ( $cnt >= 4)
			{
	            throw new \yii\web\HttpException (400, 'Не больше 4 игроков!' );
			}
			
            $gameplayer = new \app\models\GamePlayer();
            $gameplayer->game_id = $game_id;
            $gameplayer->player_id = $player_id;
            $gameplayer->score = 0;
            $gameplayer->save();
            $newgameplayer = \app\models\GamePlayer::findOne($gameplayer->id);
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $newgameplayer->toArray();
        }
        else
        {
            throw new \yii\web\HttpException (404, 'Не удалось добавить пользователя' );
        }
        
    }
    public function actionDelPlayer($player_id)
    {
        $del = \app\models\GamePlayer::deleteAll ( [ 'game_id' => $this->gameID(), 'player_id' => $player_id ] );
        if ( 1 === $del )
        {
            $del = \app\models\Results::deleteAll ( [ 'game_id' => $this->gameID(), 'player_id' => $player_id ] );
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['player_id' => $player_id];
        }
        else
        {
            throw new \yii\web\HttpException (404, 'Не удалось удалить пользователя' );
        }

   }
    
    public function actionLog()
    {
        $this->gameID();
        $steps = models\Results::find()->select("DISTINCT `step`")->orderBy('`step`')->all();
        $res = [];
        foreach ( $steps as $step )
        {
			$results = models\Results::find()->where(['step' => $step->step, 'game_id' => $this->gamestate->current_game_id])->all();
            foreach ( $results as $result )
            {
				$player = models\Player::findOne ( $result->player_id );
				$logitem = $result->toArray();
				$logitem['name'] = $player->name;
                $res[$step->step][] = $logitem;
            }
			for ( $i = sizeof ( $res ) - 1; $i >= 0; $i -- )
			{
				if ( !isset ( $res[$i] ))
				{
					$res[$i] = [];
				}
			}
        }
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return $res;
    }
    
    public function actionAddScore($player_id, $score)
    {
        $gameplayer = models\GamePlayer::find()->where(['game_id' => $this->gameID(), 'player_id' => $player_id])->one();
        if ( null !== $gameplayer )
        {
            $result = models\Results::find()->where(['game_id' => $this->gameID(), 'player_id' => $player_id, 'step' => $this->gamestate->current_step ])->one();
            if ( null !== $result )
            {
				$gameplayer->score -= $result->score;
				if ( 0 === (int)$score )
				{
					$result->delete();
				}
				else
				{
					$result->score = $score;
					$result->save();
				}
            }
            else
            {
				if ( 0 !== (int)$score )
				{
					$results = new models\Results ();
					$results->game_id = $this->gamestate->current_game_id;
					$results->player_id = $player_id;
					$results->step = $this->gamestate->current_step;
					$results->score = $score;
					$results->save();
				}
            }
            $gameplayer->score += $score;
            $gameplayer->save();
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return $gameplayer->toArray ();
        }
        else
        {
            throw new \yii\web\HttpException ( 404, 'Не удалось добавить очки' );
        }
    }    
    
    public function actionNextStep()
    {
        $this->gameID();
        $this->gamestate->current_step++;
        $this->gamestate->save();
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['step' => $this->gamestate->current_step ];
    }
    public function actionPrevStep()
    {
        $this->gameID();
		if ( $this->gamestate->current_step > 1 )
		{
			$this->gamestate->current_step--;
			$this->gamestate->save();
		}
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['step' => $this->gamestate->current_step ];
    }
    
}
