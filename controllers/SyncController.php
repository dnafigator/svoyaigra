<?php

namespace app\controllers;

use yii\web\Controller;
class SyncController extends Controller
{
    
    var $cfg = null;
	
	
	public function behaviors()
	{
		return [
			'access' => [
				'class' => \yii\filters\AccessControl::className(),
				'only' => ['index'],
				'rules' => [
					// allow authenticated users
					[
						'allow' => true,
						'roles' => ['@'],
					],
					// everything else is denied
				],
			],            
		];
	}	
	
    
    public function hash( $playground, $game_id, $secret )
    {
        return md5 ( $playground.$game_id.$secret);
    }

    public function loadCfg()
    {
        $this->cfg = require ( __DIR__.'/../config/playgrounds.php');
    }
    
    public function checkHash( $playground, $game_id, $hash )
    {
        return $hash == $this->hash( $playground, $game_id, $this->cfg['secrets'][ $playground ] );
    }
    
    public function beforeAction($action)
    {
        if ($action->id === 'receive') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }
	
    public function actionIndex()
	{
		$gm = [];
		$games = \app\models\Game::find()->all();
		foreach ( $games as $game )
		{
			$gm[$game->id] = 'Игра '.$game->id.' '.$game->create_date.((null === $game->end_date)?'':' – '.$game->end_date);
		}
		return $this->render ( 'index', [ 'games' => $gm ] );
	}
	
    public function actionPush($game_id)
    {
		$gs = \app\models\Gamestate::find()->one ();
        $gps = \app\models\GameResult::find()->where ( ['game_id' => $game_id, 'playground' => $gs->playground ] )->all();
		if ( sizeof ( $gps ) )
		{
			$results = [];
			foreach ( $gps as $gp )
			{
				$results[] = $gp->toArray();
			}
			$ret = [
				'pg' => $gs->playground,
				'game_id' => $game_id,
				'hash' => $this->hash($gs->playground, $game_id, $gs->playground_secret),
				'results' => $results
			];
			//*/comment this for debug
			$this->loadCfg();
			$client = new \GuzzleHttp\Client(['base_url' => $this->cfg['central'], 'timeout' => 2000 ]);
			$response = $client->post ( '/sync/receive', ['body' => json_encode($ret)] );
			$ret = $response->json();
			//*/
		}
		else
		{
			$ret = ['success' => false, 'error' => 'Нет результатов для игры '.$game_id ];
		}
		
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		return $ret;
    }
    
	public function actionReceive()
	{
		$this->loadCfg();
		$json = json_decode(\Yii::$app->request->rawBody);
		if ($this->checkHash($json->pg, $json->game_id, $json->hash))
		{
			\app\models\GameResult::deleteAll(['playground' => $json->pg, 'game_id' => $json->game_id]);
			foreach ($json->results as $result)
			{
				$rec = new \app\models\GameResult ();
				$rec->playground = $result->playground;
				$rec->game_id = $result->game_id;
				$rec->create_date = $result->create_date;
				$rec->end_date = $result->end_date;
				$rec->player_id = $result->player_id;
				$rec->name = $result->name;
				$rec->rating = $result->rating;
				$rec->save();
			}
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			return ['success' => true];
		} else {
			throw new \yii\web\HttpException(403, 'Вы кем-то притворяетесь');
		}
	}
}	